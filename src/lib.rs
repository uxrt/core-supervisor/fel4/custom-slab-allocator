// Copyright 2021-2024 Andrew Warkentin
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

#![no_std]

pub trait CustomSlabAllocator {
	///Adds a custom slab to the allocator
	///
	///See Heap::add_custom_slab() and DynamicSlabInfo in
	///slab-allocator-core for more information on the arguments.
	///
	///Panics if the allocation fails (the description is included in the
	///panic message.
	fn add_custom_slab(&self, block_size: usize, slab_size: usize, min_free: usize, max_dealloc_slabs: usize, max_dealloc_rounds: usize, description: &str);
	///Adds a custom slab to the allocator on the next allocation. The block
	///size will be the size of the next allocation. All other arguments are
	///the same as for the add_custom_slab()
	fn add_next_custom_slab(&self, slab_size: usize, min_free: usize, max_dealloc_slabs: usize, max_drop_rounds: usize, description: &str);
}

///Adds a slab size for an Arc of the given type.
///
///Arguments are similar to CustomSlabAllocator::add_custom_slab, although the
///block size is specified as a type (without the Arc).
///
///This is a macro since it uses a fixed array as a substitute, and it is not
///possible to use the size of a generic type as a constant to define/initialize
///an array (which would require generic statics)
#[macro_export]
macro_rules! add_arc_slab {
	($alloc: expr, $slab_type: ty, $slab_size: expr, $min_free: expr, $max_dealloc_slabs: expr, $max_drop_rounds: expr, $description: expr) => {
		{
		use ::custom_slab_allocator::CustomSlabAllocator;
		fn add_arc_slab<A: CustomSlabAllocator>(slab_alloc: &A, slab_size: usize, min_free: usize, max_dealloc_slabs: usize, max_drop_rounds: usize, description: &str) {
			use ::core::mem::size_of;
			use ::alloc::sync::Arc;

			const BLOCK_SIZE: usize = size_of::<$slab_type>();
			let array: [u8; BLOCK_SIZE] = [0; BLOCK_SIZE];
			slab_alloc.add_next_custom_slab(slab_size, min_free, max_dealloc_slabs, max_drop_rounds, description);
			let arc = Arc::new(array);
			drop(arc);
		}
		add_arc_slab($alloc, $slab_size, $min_free, $max_dealloc_slabs, $max_drop_rounds, $description);
		}
	}
}
